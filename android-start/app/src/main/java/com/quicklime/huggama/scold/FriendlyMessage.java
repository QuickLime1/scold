/**
 * Copyright Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.quicklime.huggama.scold;

public class FriendlyMessage {

    private String id;
    private double rate;
    private String name;
    private String photoUrl;
    private long phone;
    private long phone1;
    private long phone2;

    public FriendlyMessage() {
    }

    public FriendlyMessage(double rate, String name, String photoUrl,long phone,long phone1,long phone2) {
        this.rate = rate;
        this.name = name;
        this.photoUrl = photoUrl;
        this.phone=phone;
        this.phone1=phone1;
        this.phone2=phone2;
    }

    public String getId() {
        return id;
    }

    public long getPhone() {return phone;}

   public long getPhone1(){return phone1;}
    public void setPhone1(long phone1){this.phone1=phone1;}
public long getPhone2(){return phone2;}
    public void setPhone2(long phone2) {
        this.phone2 = phone2;
    }
    public void setPhone(long phone){this.phone=phone;}

    public void setId(String id) {
        this.id = id;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public double getRate() {
        return rate;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
